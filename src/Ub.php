<?php
    class Ub implements Command{
        private $query;

        public function __construct($query = false){
            $this->query = str_replace(' ','+', $query); // replace spaces with + for the GET request.
        }

        public function run($channel){
            $output = Ub::QueryAPI($this->query);

            // $tags = implode(', ',$output->tags);
            if($output->result_type == 'no_results'){
                $conn = Connection::getInstance();
                $conn->SendData($channel,'Sorry. No results on Urban dictionary for '.str_replace('+',' ',$this->query));
            }
            else{
                $definition = $output->list;
                $definition = $definition[0]->definition;
                $permalink = $output->list[0]->permalink;

                $conn = Connection::getInstance();
                $conn->SendData($channel,'11[ '.$definition.' - '.$permalink.' ]');
                // $conn->SendData($channel,'Tags: '.$tags);
            }
        }

        public function QueryAPI($term){
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,'http://api.urbandictionary.com/v0/define?term='.$term);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $output = curl_exec ($ch);
            curl_close ($ch);
            $output = json_decode($output);

            return $output;
        }
    }
?>