<?php
    class Log implements Command{
        private $date;

        public function __construct($date = false){
            $this->date = $date;
        }

        public function run($channel){
            switch($this->date){
                case '':
                    $this->date = date('Y-m-d');
                    break;
                case 'today':
                    $this->date = date('Y-m-d');
                    break;
                case 'yesterday':
                    $this->date = date('Y-m-d', time() - 60 * 60 * 24);
                    break;
            }

            $logurl = shell_exec('cat logs/'.escapeshellarg($this->date).'.csv | nc dailyprog.org 6969 2>&1');

            $conn = Connection::getInstance();
            $conn->SendData($channel,$logurl);
        }
    }
?>
