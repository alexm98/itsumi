<?php
    class Logger{
        private $log;

        private function __construct(){
        }

	public function run(){
	    // Should not be implemented for this is not a usable function by a user.
	}

        public function getInstance(){
            static $instance;

            if ($instance === null) {
                $instance = new Logger();
            }

            return $instance;
        }

        public function addHeader(){
            $log = array_map('str_getcsv',file('./logs/'.date("Y-m-d").'.csv'));

            if($log[0][0] != 'timestamp'){
                fputcsv($this->log,['timestamp','user','message']);
            }
        }

        public function logMessage(Message $msg){
            Logger::setCurrentLog();

            if($msg->getSender() !== null && $msg->getVerb() != 'JOIN' || $msg->getVerb() != 'QUIT'){
                $blacklist = require('log_blacklist.php');
                if(! in_array($msg->getSender(),$blacklist)){
                    $finalmsg = array(addslashes($msg->timestamp),$msg->getSender(),Logger::escapeSimpleAndDoubleSlashes($msg->message));

                    fputcsv($this->log,$finalmsg);
                    fclose($this->log);
                }
            }
        }

        public function setCurrentLog(){
            $this->log = fopen('./logs/'.date("Y-m-d").'.csv','a');

            Logger::addHeader();
        }

        // also removes non-printable characters with the preg_replace.
        function escapeSimpleAndDoubleSlashes($msg){
            $message = addslashes(rtrim($msg));
            // $message = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $message);
            // UTF-8
            // $message = preg_replace('/[\x00-\x1F\x7F]/u', '', $message);

            return $message;
        }
    }
?>
