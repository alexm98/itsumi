<?php ini_set('user_agent','w3m/0.5.3+cvs-1.1055');
    class Linkanalyzer{
	public function  run(){
	    // Should not be implemented for this is not a usable function by a user.
	}

        public function match($message){
            $regex = '(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})';
            preg_match($regex,$message,$link);
            @$link = $link[0];

            if(isset($link)){
                return $link;
            }

            return 0;
        }

        public function appendProtocol($link){
            if(substr($link,0,3) == 'www'){
                $link = 'http://'.$link;
            }

            $link = trim($link);

            return $link;
        }

        public function isBlacklisted($url){
            $url_blacklist = file('blacklist.txt', FILE_IGNORE_NEW_LINES);

            if(in_array($url,$url_blacklist)){
                return 1;
            }

            return 0;
        }

        public function analyze($channel,$link){
            $link = Linkanalyzer::appendProtocol($link);

            if(!Linkanalyzer::isBlacklisted($link)){
                $conn = Connection::getInstance();
                $page = file_get_contents($link);
                $purple = '02';

                $html = new simple_html_dom();
                $html->load($page);

                @$descr = $html->find("meta[name='description']", 0,true)->content;
                @$title = $html->find('title',0)->innertext;
		$descr_length = 256;

                $descr = html_entity_decode($descr, ENT_QUOTES | ENT_XML1 | ENT_HTML5, 'UTF-8');

		if(strlen($descr) > $descr_length){
		    $descr = substr($descr,0,$descr_length)."...";
		}

                $title = html_entity_decode($title, ENT_QUOTES | ENT_XML1 | ENT_HTML5, 'UTF-8');
                $header = get_headers($link);
                $content_type = explode(';',substr(array_values(preg_grep('/(Content-Type:)/',$header))[0],14),2)[0];

                if($content_type == 'text/html'){
                    if($title != '' && $descr != ''){
                        $conn->sendData($channel,$purple.'[ '.$title.' - '.$descr.' ]');
                    }
                    else if($descr == ''){
                        $conn->sendData($channel,$purple.'[ '.$title.' ]');
                    }
                    else if($title == ''){
                        $conn->sendData($channel,$purple.'[ '.$descr.' ]');
                    }
                }

                else if(substr($content_type,0,5) == 'image'){
                    $image_type = substr($content_type,6);
                    $image_size = $image_type.' image, ';
                    $image_size.= substr(array_values(preg_grep('/(Content-Length)/',$header))[0],16);

                    $conn->sendData($channel,$image_size.' bits');
                }
            }
        }
    }
?>
