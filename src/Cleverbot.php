<?php
    class Cleverbot implements Command{
        private $query;
        private $nick;

        public function __construct($query = false){
            $this->query = $query;
        }

        public function run($channel){
            $config = require('config.php');
            $this->nick = Cleverbot::QueryAPI('create',"user={$config['cleverbot_userkey']}&key={$config['cleverbot_secret']}")->nick;
            $output = Cleverbot::QueryAPI('ask',"user={$config['cleverbot_userkey']}&key={$config['cleverbot_secret']}&nick={$this->nick}&text={$this->query}");

            if($output->status == 'success'){
                $conn = Connection::getInstance();
                $conn->SendData($channel,'Cleverbot says: '.$output->response);
            }
        }

        public function QueryAPI($url,$params){
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,'https://cleverbot.io/1.0/'.$url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $output = curl_exec ($ch);
            curl_close ($ch);
            $output = json_decode($output);

            return $output;
        }
    }
?>