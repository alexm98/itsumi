<?php
    class Message{
        private $sender;
        private $verb;
        private $channel;
        public $timestamp;
        public $message;

        public function __construct($msgdata){
            $msgdata = explode(' ',$msgdata);
            $this->sender = ltrim(explode('!',$msgdata[0])[0],':');
            $this->verb = $msgdata[1];
            @$this->channel = $msgdata[2];
            @$this->message = rtrim(ltrim(str_replace(["\r","\n"], '', implode(' ',array_slice($msgdata,3))),':'));
            $this->timestamp = date('r');
        }

	public function run(){
	    // Should not be implemented for this is not a usable function by a user.
	}

        public function toString(){
            echo $this->timestamp.' '.$this->sender.' '.$this->verb.' '.$this->channel.' '.$this->message."\n";
        }

        // In case of a PING command from the IRC server, the sender is "PING"
        public function getSender(){
            if(isset($this->sender)){
                return $this->sender;
            }
        }

        public function getVerb(){
            return $this->verb;
        }

        public function getChannel(){
            $config = include('config.php');

            if($this->channel == $config['nickname']){
                return $this->getSender();
            }
            else{
                return $this->channel;
            }
        }

        public function remove_end(Message $m){
            $cleardata = str_replace(["\r","\n"], '', $m);

            return $cleardata;
        }
    }
?>
