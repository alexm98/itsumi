<?php ini_set('user_agent','Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Thunderbird/45.3.0');
    class Search{
        public $query;

        public function __construct($query = false){
            $this->query = $query;
        }

        public function run($channel){
            $this->query = str_replace(' ','+',$this->query);
            $output = file_get_contents('https://duckduckgo.com/html/?t=hf&ia=web&q='.$this->query);

            $html = new simple_html_dom();
            $html->load($output);

            $link = $html->find('/html/body/div[1]/div[3]/div/div/div[1]/div/h2/a');
            $link = explode('ddg=',urldecode($link[0]->href))[1];

            if(!isset($link)){
                $link = $html->find('/html/body/div[1]/div[4]/div/div/div[1]/div/h2/a');
                $link = explode('ddg=',urldecode($link[0]->href))[1];
            }

            if(strlen($link) > 20){
                $link = Search::minifyURL($link);
            }

            $conn = Connection::getInstance();
            $conn->SendData($channel,$link);
        }

        public function minifyURL($url){
            return file_get_contents('http://tinyurl.com/api-create.php?url='.urlencode($url));
        }
    }
?>