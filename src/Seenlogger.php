<?php 
    class Seenlogger{
        public function userAlreadyLogged($user){
	    $db = new PDO('sqlite:./seenlog.sqlite3'); 
            $statement = $db->prepare("SELECT COUNT(nick) FROM log WHERE nick LIKE :user");
            $statement->execute([':user' => $user]);
            $result = $statement->fetchAll();

            if($result[0][0] == 0){
                return 0;
            }
            else{
                return 1;
            }
        }

        public function logUser($user){
            $date = date('r');

            if(Seenlogger::userAlreadyLogged($user)){
                $db = new PDO('sqlite:./seenlog.sqlite3');
                $stmt = $db->prepare('UPDATE log SET seen = :date WHERE nick LIKE :user');
                $stmt->execute([':date' => $date, ':user'=>$user]);
            }
            else{
                $db = new PDO('sqlite:./seenlog.sqlite3');
                $stmt = $db->prepare('INSERT INTO log VALUES(:user, :seen)');
                $stmt->execute([':user' => $user, ':seen' => $date]);
            }
        }
    }
?>
