<?php
class Help implements Command{
    private $query;

    public function __construct($query = false){
        $this->query = $query;
    }

    function run($channel){
        $command = $this->query;
        $conn = Connection::getInstance();

        if($command != ''){
            $commandsfile = file_get_contents('./commandsman.json');
            $data = json_decode($commandsfile);

            $command = [$data->$command][0];
            if(is_array($command)){
                foreach($command as $line){
                    Connection::SendData($channel,"8".$line);
                }
            }
            else{
                Connection::SendData($channel,"8".$command);
            }
        }

        else{
            $commands = ['!help','!dpt','!alive','!color','!time','!log','!seen'];
            $commands = 'Here is a list of available commands: '.implode(' ',$commands);

            Connection::SendData($channel,"8".$commands);
        }
    }
}
?>