<?php
    class Wolfram implements Command{
        private $query;

        public function __construct($query = false){
            $this->query = $query;
        }

        public function run($channel){
            $config = require('config.php');

            $query = preg_replace('/\+/','plus',$this->query);
            $ch = curl_init();

            curl_setopt($ch,CURLOPT_URL,"http://api.wolframalpha.com/v1/conversation.jsp");
            curl_setopt($ch,CURLOPT_POST, 1);
            curl_setopt($ch,CURLOPT_POSTFIELDS, "appid={$config['wolfram_appid']}&i={$this->query}");
            curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

            $output = curl_exec($ch);
            curl_close($ch);

            $output = json_decode($output);
            if(isset($output->result)){
                $conn = Connection::getInstance();
                $conn->SendData($channel,'Wolfram: '.$output->result);
            }
        }
    }
?>
