<?php
    class Ponger{
	public function  run(){
	    // Should not be implemented for this is not a usable function by a user.
	}

        public function pongBack($socket,$optdata = false){
            if($optdata){
                fputs($socket, "PONG ".$optdata."\n");
            }
            else{
                $data = fgets($socket, 128);
                $msg = explode(' ', $data);

                fputs($socket, "PONG ".$msg[1]."\n");
            }

            flush();
        }
    }
?>
