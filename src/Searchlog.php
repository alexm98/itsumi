<?php
    class Searchlog implements Command{
        private $query;

        public function __construct($query = false){
            $this->query = $query;
        }

        public function run($channel){
            $conn = Connection::getInstance();
            $bold = '';

            $lastthreeresults = shell_exec('cat logs/*.csv | grep -v !searchlog | grep '.escapeshellarg($this->query).'| tail -3');
            $lastthreeresults = str_replace('"','',$lastthreeresults);
            $lastthreeresults = str_replace($this->query,$bold.$this->query.$bold,$lastthreeresults);
            $lastthreeresults = explode("\n",$lastthreeresults);

            foreach($lastthreeresults as $result){
                $conn->SendData($channel,$result);
                sleep(1);
            }
        }
    }
?>