<?php
    class Color implements Command{
        private $payload;

        public function __construct($payload= false){
            $this->payload= $payload;
        }

        public function run($channel){
            $coloredtext = '';

            for($i=0; $i < strlen($this->payload); $i++){
                $coloredtext .= ''.rand(2,15).$this->payload[$i];
            }

            $conn = Connection::getInstance();
            $conn->SendData($channel,$coloredtext);
        }
    }
?>