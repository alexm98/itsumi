<?php
    class Seen implements Command{
        public $username;

        public function __construct($user){
            $this->username = $user;
        }

        public function run($channel){
            $db = new PDO('sqlite:./seenlog.sqlite3');
            $statement = $db->prepare("SELECT * FROM log WHERE nick LIKE :user");
            $statement->execute([':user'=>$this->username]);
            $results = $statement->fetchAll();

            if(isset($results[0][0])){
                $conn = Connection::getInstance();
                $conn->sendData($channel,"User ".$results[0][0]." was last seen on: ".$results[0][1]."\n");
            }
        }
    }
?>
