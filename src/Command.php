<?php
    interface Command{
        public function run($context);
    }
?>
