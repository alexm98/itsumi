<?php
    class Connection{
        private $socket;
        public $config;

        private function __construct(){
            // set_time_limit(0);
            $this->config = include('config.php');
            $this->socket = fsockopen($this->config['network'], 6667);
            echo "Connected\n";
        }

	public function  run(){
	    // Should not be implemented for this is not a usable function by a user.
	}

        public function GetInstance(){
            static $instance;

            if ($instance === null) {
                $instance = new Connection();
            }

            return $instance;
        }

        public function AuthAndJoinIRC(){
            $socket = Connection::getInstance()->socket;

            fputs($socket,"USER ".$this->config['nickname']." 0 * :".$this->config['master']."'s Bot\n");
            fputs($socket,"NICK ".$this->config['nickname']."\n");

	    // pong before joining channel
            for($logincount = 0; $logincount < 10; $logincount++){
                Ponger::pongBack($this->socket);
            }

            fputs($socket,"MSG NICKSERV IDENTIFY".$this->config['password']."\n");

	    foreach($this->config['channels'] as $channel){
		fputs($socket,"JOIN ".$channel."\n");
	    }
        }

        public function GetData(){
            $socket = Connection::getInstance()->socket;

            while(1){
                while($data = fgets($socket, 500)){
                    $msg = explode(' ', $data);

                    $currentmsg = new Message($data);
                    $currentmsg->toString();

                    $analyzer = new Linkanalyzer();
                    if($url = $analyzer->match($currentmsg->message)){
                        $analyzer->analyze($currentmsg->getChannel(),$url);
                    }
                    /* flush();*/

                    $logger = Logger::getInstance();
                    $logger->logMessage($currentmsg);

                    // in case of a PING, the sender itself is PING and the message to send back is the verb...
                    if($currentmsg->getSender() == 'PING'){
                        Ponger::pongBack($this->socket,$currentmsg->getVerb());
                    }

                    if($currentmsg->getVerb() == 'PART' || $currentmsg->getVerb() == 'QUIT'){
                        $slogger = new Seenlogger();
                        $slogger->logUser($currentmsg->getSender());
                    }

                    if($currentmsg->getVerb() == 'JOIN'){
                        $slogger = new Seenlogger();
                        if($slogger->userAlreadyLogged($currentmsg->getSender()) == 0){
                            Connection::sendData($this->config['channel'],"Welcome to ".$this->config['channel'].", ".$currentmsg->getSender()."! \n");
                        }
                    }

                    if($currentmsg->getSender() != 'PING' && ISSET($currentmsg->message) && @$currentmsg->message[0] == '!'){
                        $cmd = explode(' ',substr($currentmsg->message,1));

                        // create class with optional param (wolfram,cleverbot) that needs user input
                        if(count($cmd) > 1){
                            $cmd[0] = ucfirst($cmd[0]);
                            if(class_exists($cmd[0])){
                                $command = new $cmd[0](implode(' ',array_slice($cmd,1)));
                                $command->run($currentmsg->getChannel());
                            }
                        }
                        else{
                            $cmd = ucfirst($cmd[0]);

                            if(class_exists($cmd)){
                                $command = new $cmd();
                                $command->run($currentmsg->getChannel());
                            }
                            else{
                                Connection::sendData($currentmsg->getChannel(),"Sorry, that command doesn't exist! Consider suggesting it at http://gitlab.com/alexm98/itsumi \n");
                            }
                        }
                    }
                }
            }
        }

        public function SendData($channel,$data){
            $socket = Connection::getInstance()->socket;

            fputs($socket, "PRIVMSG ".$channel." :".$data."\n");
        }
    }
?>
