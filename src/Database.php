<?php
    // class not yet in use, sqlite3 wrapper wannabe.
    class Database{
        public $config;
        public $db;

        private function __construct(){
            $this->config = include('config.php');
            $this->db = new PDO('sqlite:./'.$this->config['database'].'sqlite3');
        }

	public function  run(){
	    // Should not be implemented for this is not a usable function by a user.
	}

        public function getInstance(){
            static $instance;

            if ($instance === null) {
                $instance = new Database();
            }

            return $instance;
        }

        public function query($q,$execargs){
            $database = Database::getInstance();
            $statement = $database->db->prepare($q);
            $statement->execute($execargs);
            $results = $statement->fetchAll();

            if(isset($results[0][0])){
                return $results;
            }
        }
    }
?>
