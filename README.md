# Itsumi
Itsumi is an IRC bot created for [/dailyprog](http://dpt.slasheva.com)  

To see it live, you can go to:  
`/server irc.rizon.net`  
`/join #/g/dpt`  
___
## Dependencies:
* php
* php-curl
* php-sqlite3
* sqlite3
